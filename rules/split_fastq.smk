__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## scatter a fastq file
rule split_fastq:
    input:
        R1=config["fichiers"]["folder_fastq"]+'{run}_R1.fastq.gz',
        R2=config["fichiers"]["folder_fastq"]+'{run}_R2.fastq.gz'
    output:
        R1='results/02b_scaterred/{run}_R1_{chunk}.fastq',
        R2='results/02b_scaterred/{run}_R2_{chunk}.fastq'
    conda:
        '../envs/fastqsplitter_envs.yaml'
    params:
        R1=lambda wildcards: '-o '+' -o '.join([ 'results/02b_scaterred/' + wildcards.run +'_R1_'+ str(i) + '.fastq' for i in listChunks]),
        R2=lambda wildcards: '-o '+' -o '.join([ 'results/02b_scaterred/' + wildcards.run +'_R2_'+ str(i) + '.fastq' for i in listChunks])
    shell:
        '''CMD="fastqsplitter -i {input.R1}"; eval $CMD {params.R1}; CMD="fastqsplitter -i {input.R2}"; eval $CMD {params.R2};'''


