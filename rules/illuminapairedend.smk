__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Paired end alignment then keep reads with quality > 40
rule illuminapairedend:
    input:
        R1=config["fichiers"]["folder_fastq"]+'{run}_R1.fastq.gz',
        R2=config["fichiers"]["folder_fastq"]+'{run}_R2.fastq.gz'
    output:
        fq='results/02_illuminapairedend/{run}.fastq'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/02_illuminapairedend/{run}.log'
    params:
        s_min=config["illuminapairedend"]["s_min"]        
    shell:
        '''illuminapairedend -r {input.R2} {input.R1} --score-min={params.s_min} > {output.fq} 2> {log}'''

