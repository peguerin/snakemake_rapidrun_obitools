__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Generate a table final results
rule table_assigned_sequences:
    input:
        'results/17_sort_abundance_assigned_sequences/{projmarkrun}.s.a.t.u.fasta'
    output:
        'results/18_table_assigned_sequences/{projmarkrun}.csv'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.projmarkrun].to_dict('records')[0],
    log:
        'logs/18_table_assigned_sequences/{projmarkrun}.log'
    shell:
        '''mkdir -p results/17_sort_abundance_assigned_sequences/{params.drun[projet]}/{params.drun[marker]};
        obitab -o {input} > {output} 2> {log}'''
