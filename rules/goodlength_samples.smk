__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## only sequence more than 20bp with no ambiguity IUAPC with total coverage greater than 10 reads
rule goodlength_samples:
    input:
        'results/09_dereplicate_samples/{sample}.uniq.fasta'
    output:
        'results/10_goodlength_samples/{sample}.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/10_goodlength_samples/{sample}.log'
    params:
        dmulti= lambda wildcards: dfMulti[dfMulti.demultiplex == wildcards.sample].to_dict('records')[0],  
        seq_count=config["good_length_samples"]["seq_count"],
        seq_length_min=config["good_length_samples"]["seq_length_min"]
    shell:
        '''
        mkdir -p results/10_goodlength_samples/{params.dmulti[projmarkrun]};
        if [[ -s {input} ]]; then obigrep  -p 'count>{params.seq_count}' -s '^[ACGT]+$' -p 'seq_length>{params.seq_length_min}' {input} > {output} 2> {log};
        else touch {output}; fi'''