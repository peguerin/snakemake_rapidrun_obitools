__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Remove sequence which are classified as 'internal' by obiclean
rule rm_internal_samples:
    input:
        'results/11_clean_pcrerr_samples/{sample}.r.l.u.fasta'
    output:
        'results/12_rm_internal_samples/{sample}.c.r.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    params:
        dmulti= lambda wildcards: dfMulti[dfMulti.demultiplex == wildcards.sample].to_dict('records')[0],
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/12_rm_internal_samples/{sample}.log'
    shell:
        '''
        mkdir -p results/12_rm_internal_samples/{params.dmulti[projmarkrun]};
        if [[ -s {input} ]]; then mkdir -p ../results/04_filter_samples/04_filtered/{params.dmulti[projmarkrun]}; obigrep -p "obiclean_internalcount == 0" {input} > {output} 2> {log} ; else touch {output} 2> {log}; fi;
        '''
