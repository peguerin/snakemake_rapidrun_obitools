__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Dereplicate and merge samples together
rule dereplicate_runs:
    input:
        'results/13_cat_samples_into_runs/{projmarkrun}.fasta'
    output:
        'results/14_dereplicate_runs/{projmarkrun}.uniq.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.projmarkrun].to_dict('records')[0],
    log:
        'logs/14_dereplicate_runs/{projmarkrun}.log'
    shell:
        '''
        mkdir -p results/14_dereplicate_runs/{params.drun[projet]}/{params.drun[marker]}
        obiuniq -m sample {input} > {output} 2> {log}
        '''