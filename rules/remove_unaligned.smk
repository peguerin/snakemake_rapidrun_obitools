__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Remove unaligned sequence records
rule remove_unaligned:
    input:
        fq='results/02_illuminapairedend/{run}.fastq'
    output:
        ali='results/03_remove_unaligned/{run}.ali.fastq'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/03_remove_unaligned/{run}.log'
    shell:
        '''obigrep -p 'mode!=\"joined\"' {input.fq} > {output.ali} 2> {log}'''
