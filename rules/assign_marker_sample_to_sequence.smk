__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Assign each sequence record to the corresponding sample/marker combination
rule assign_marker_sample_to_sequence:
    input:
        'results/05_demultiplex_flags/{demultiplex}.flag'
    output:
        assign='results/06_assign_marker_sample_to_sequence/{demultiplex}.ali.assigned.fastq',
        unid='results/06_assign_marker_sample_to_sequence/{demultiplex}.unidentified.fastq'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:        
        dmulti= lambda wildcards: dfRunMarker[dfRunMarker.projMarkRun == wildcards.demultiplex].to_dict('records')[0],
    log:
        'logs/06_assign_marker_sample_to_sequence/{demultiplex}.log'
    shell:
        '''ngsfilter -t {params.dmulti[dat]} -u {output.unid} results/03_remove_unaligned/{params.dmulti[run]}.ali.fastq --fasta-output > {output.assign} 2> {log}'''

