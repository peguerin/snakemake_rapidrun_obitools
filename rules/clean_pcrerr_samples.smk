__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Clean the sequences for PCR/sequencing errors (sequence variants)
rule clean_pcrerr_samples:
    input:
        'results/10_goodlength_samples/{sample}.l.u.fasta'
    output:
        'results/11_clean_pcrerr_samples/{sample}.r.l.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/11_clean_pcrerr_samples/{sample}.log'
    params:
        dmulti= lambda wildcards: dfMulti[dfMulti.demultiplex == wildcards.sample].to_dict('records')[0],
        r=config["clean_pcrerr_samples"]["r"]
    shell:
        '''
        mkdir -p results/11_clean_pcrerr_samples/{params.dmulti[projmarkrun]};
        if [[ -s {input} ]]; then obiclean -r {params.r} {input} > {output} 2> {log}; else touch {output} 2> {log}; fi'''