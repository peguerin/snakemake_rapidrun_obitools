__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## On scattered fastq chunk Paired end alignment then keep reads with quality > 40
checkpoint illuminapairedend:
    input:
        R1='results/02b_scaterred/{run}_R1_{chunk}.fastq',
        R2='results/02b_scaterred/{run}_R2_{chunk}.fastq'
    output:
        fq='results/02_illuminapairedend/{run}_{chunk}.fastq'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/02_illuminapairedend/{run}_{chunk}.log'
    params:
        s_min=config["illuminapairedend"]["s_min"],
        gathered=lambda wildcards: 'results/02_illuminapairedend/' + wildcards.run + '.fastq',
    shell:
        '''illuminapairedend -r {input.R2} {input.R1} --score-min={params.s_min} > {output.fq} 2> {log};'''

## function to get list of chunk files for each run
def get_chunks(wildcards):    
    meschunks = expand(rules.illuminapairedend.output.fq, **wildcards, chunk=listChunks)
    return meschunks

## gather `chunk` files into a single `run` fastq file
rule merge_chunks:
    input:
        get_chunks
    output:
        fq='results/02_illuminapairedend/{run}.fastq'
    shell:
        '''cat {input} > {output}'''


