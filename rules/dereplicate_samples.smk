__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"

## dereplicate reads into uniq sequences
rule dereplicate_samples:
    input:
        'results/08_samples/{sample}.fasta'
    output:
        'results/09_dereplicate_samples/{sample}.uniq.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/09_dereplicate_samples/{sample}.log'
    params:
        dmulti= lambda wildcards: dfMulti[dfMulti.demultiplex == wildcards.sample].to_dict('records')[0],        
    shell:
        ''' mkdir -p results/09_dereplicate_samples/{params.dmulti[projmarkrun]};
            if [[ -s {input} ]]; then obiuniq -m sample {input} > {output} 2> {log}; else touch {output}; fi;'''