__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## The sequences can be sorted by decreasing order of count
rule sort_abundance_assigned_sequences:
    input:
        'results/16_remove_annotations/{projmarkrun}.a.t.u.fasta'
    output:
        'results/17_sort_abundance_assigned_sequences/{projmarkrun}.s.a.t.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.projmarkrun].to_dict('records')[0],
    log:
        'logs/17_sort_abundance_assigned_sequences/{projmarkrun}.log'
    shell:
        '''
        mkdir -p results/17_sort_abundance_assigned_sequences/{params.drun[projet]}/{params.drun[marker]};
        obisort -k count -r {input} > {output} 2> {log}'''