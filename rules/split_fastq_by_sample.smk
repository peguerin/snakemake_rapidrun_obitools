__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


### Split the input sequence file in a set of subfiles according to the values of attribute `sample`
rule split_fastq_by_sample:
    input:
        'results/06_assign_marker_sample_to_sequence/{demultiplex}.ali.assigned.fastq'
    output:
        'results/07_split_fastq_by_sample/flags/{demultiplex}.done'
    params:
        dir='results/07_split_fastq_by_sample/{demultiplex}/'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    log:
        'logs/07_split_fastq_by_sample/{demultiplex}.log'
    shell:
        '''mkdir -p {params.dir}; obisplit -p "{params.dir}" -t sample --fasta {input} 2> {log}; mkdir -p 07_split_fastq_by_sample/flags/; touch {output};'''
