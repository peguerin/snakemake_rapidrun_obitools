__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Assign each sequence to a taxon
rule taxonomic_assignment:
    input:
        'results/14_dereplicate_runs/{projmarkrun}.uniq.fasta'
    output:
        'results/15_taxonomic_assignment/{projmarkrun}.tag.u.fasta'
    threads:
        workflow.cores * 0.5
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.projmarkrun].to_dict('records')[0],
    log:
        'logs/15_taxonomic_assignment/{projmarkrun}.log'
    shell:
        '''
        mkdir -p results/15_taxonomic_assignment/{params.drun[projet]}/{params.drun[marker]};
        ecotag -d {params.drun[bdr]} -R {params.drun[fasta]} {input} > {output} 2> {log}
        '''