__author__ = "Pierre-Edouard Guerin"
__license__ = "MIT"


## Some unuseful attributes can be removed at this stage
rule remove_annotations:
    input:
        'results/15_taxonomic_assignment/{projmarkrun}.tag.u.fasta'
    output:
        'results/16_remove_annotations/{projmarkrun}.a.t.u.fasta'
    conda:
        '../envs/obitools_envs.yaml'
    singularity:
        config["singularity"]["obitools"]
    params:
        drun= lambda wildcards: dfpmr[dfpmr.projmarkrun == wildcards.projmarkrun].to_dict('records')[0],
    log:
        'logs/16_remove_annotations/{projmarkrun}.log'
    shell:
        '''
        mkdir -p results/16_remove_annotations/{params.drun[projet]}/{params.drun[marker]};
        obiannotate --delete-tag=scientific_name_by_db --delete-tag=obiclean_samplecount \
 --delete-tag=obiclean_count --delete-tag=obiclean_singletoncount \
 --delete-tag=obiclean_cluster --delete-tag=obiclean_internalcount \
 --delete-tag=obiclean_head  --delete-tag=obiclean_headcount \
 --delete-tag=id_status --delete-tag=rank_by_db --delete-tag=obiclean_status \
 --delete-tag=seq_length_ori --delete-tag=sminL --delete-tag=sminR \
 --delete-tag=reverse_score --delete-tag=reverse_primer --delete-tag=reverse_match --delete-tag=reverse_tag \
 --delete-tag=forward_tag --delete-tag=forward_score --delete-tag=forward_primer --delete-tag=forward_match \
 --delete-tag=tail_quality {input} > {output} 2> {log}'''